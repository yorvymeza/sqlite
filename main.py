import sqlite3

# create empty database
connection = sqlite3.connect("gta.db")
# communicate with the database
cursor = connection.cursor()

# data to be inserted into the database
release_list = [
    (1997, "Grand Theft Auto", "state of New Guernsey"),
    (1999, "Grand Theft Auto 2", "Anywhere, USA"),
    (2001, "Grand Theft Auto III", "Liberty City"),
    (2002, "Grand Theft Auto: Vice City", "Vice City"),
    (2004, "Grand Theft Auto: San Andreas", "state of San Andreas"),
    (2008, "Grand Theft Auto IV", "Liberty City"),
    (2013, "Grand Theft Auto V", "Los Santos")
]

city_list = [
    ("Liberty City", "New York"),
    ("state of New Guernsey", "state of New Jersey"),
    ("Anywhere, USA", "all USA cities"),
    ("Vice City", "Miami"),
    ("state of San Andreas", "state of California"),
    ("Los Santos", "Los Angeles")
]

# Despues nos vamos al terminal y ejecutamos el siguiente 
# comando cd ../../ para acceder al directorio 






# terminate the connection to "gta.db"
# Es para cerrar la base de DB
connection.close()